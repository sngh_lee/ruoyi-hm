
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi-HM v1.0.0</h1>
<h4 align="center">若依移动端框架（鸿蒙版）</h4>

## 平台简介

RuoYi-HM APP解决方案，采用华为鸿蒙框架使用ArkTs和TS语言开发，实现了与[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue)对接的移动鸿蒙版解决方案！目前已经实现登录、我的、工作台、等基础功能。

## 版本说明
* master分支基于鸿蒙NEXT版本开发，支持API12，开发文档参考 [项目开发手册-鸿蒙NEXT版](https://luo-ji-xiang-liang-ke-ji.gitbook.io/ruo-yi-yi-dong-duan-hong-meng-ban/v/ruo-yi-yi-dong-duan-hong-meng-next-ban)
* v1.0基于鸿蒙4.0版本开发，支持API9（旧版本不再更新）

## 更新内容
* 新增商城实例模版，具体参见系统商城实例
* 新增表单服务器端验证
* 新增表单前端验证规则，具体参见表单验证示例

## 技术文档
<!-- TOC -->
  * 鸿蒙开发环境搭建 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/environment-0000001053662422-V3)
  * 鸿蒙开发快速入门 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/start-overview-0000001478061421-V3)
  * 工程编译构建 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/build_overview-0000001055075201-V3)
  * ArkUI开发入门 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/arkts-ui-development-overview-0000001438467628-V3)
  * 项目开发手册 [项目开发手册-鸿蒙4.0版](https://luo-ji-xiang-liang-ke-ji.gitbook.io/ruo-yi-yi-dong-duan-hong-meng-ban/)  [项目开发手册-鸿蒙NEXT版](https://luo-ji-xiang-liang-ke-ji.gitbook.io/ruo-yi-yi-dong-duan-hong-meng-ban/v/ruo-yi-yi-dong-duan-hong-meng-next-ban)
<!-- TOC -->

## 项目文档访问不了怎么办？
* 访问以下链接 [下载访问工具](https://honghai.xn--cesw6hd3s99f.com/#/register?code=2RzKnYOD) 
* 自行注册并安装

## 演示图

| ![](/screenshots/1.png) | ![](/screenshots/2.png) | ![](/screenshots/3.png) |
|-------------------------|-------------------------|-------------------------|
| ![](/screenshots/4.png) | ![](/screenshots/5.png) | ![](/screenshots/6.png) |


 