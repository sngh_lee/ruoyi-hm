import CookieStore from '../store/CookieStore'
import SessionStore from '../store/SessionStore'
import { UserState } from './UserState'
import { Log } from '../utils/log'

export function getUserState(): UserState {
  return SessionStore.get('user')
}

export function setUserState(userState: UserState) {
  SessionStore.set('user', userState);
}

export function delUserState() {
  SessionStore.remove('user')
}

export function setToken(token: string) {
  CookieStore.setToken(token)
}

export function getToken() {
  return CookieStore.getToken();
}

export async function delToken() {
  CookieStore.deleteToken();
}