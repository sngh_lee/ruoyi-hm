import distributedKVStore from '@ohos.data.distributedKVStore';
import CookieStore from './CookieStore';
import SessionStore from './SessionStore';
import { globalCfg } from '../../config/setting'
import { Log } from '../utils/log'

/**
 * 键值对数据库管理器
 */
class StoreManager {
  private kvManager: distributedKVStore.KVManager;
  private cookieStore: CookieStore;
  private sessionStore: SessionStore;

  constructor(context) {
    this.createManager(context);
  }

  getCookieStore() {
    return this.cookieStore;
  }

  getSessionStore() {
    return this.sessionStore;
  }

  //创建管理器
  createManager(context) {
    const kvManagerConfig = {
      context: context,
      bundleName: globalCfg.bundleName
    };
    /*
    try {
      // 创建KVManager实例
      this.kvManager = distributedKVStore.createKVManager(kvManagerConfig);
      Log.info('KVManager创建成功');
      // 继续创建数据库
      this.cookieStore = new CookieStore(this.kvManager);
      this.sessionStore = new SessionStore(this.kvManager);
    } catch (e) {
      Log.error(`KVManager创建失败. Code:${e.code},message:${e.message}`);
    }
    */
  }
}

export function createStoreManager(context) {
  let storeManger;
  if (!globalThis.StoreManager) {
    storeManger = new StoreManager(context);
    globalThis.StoreManager = storeManger;
  }
}

export function getStoreManager(): StoreManager {
  return globalThis.StoreManager;
}