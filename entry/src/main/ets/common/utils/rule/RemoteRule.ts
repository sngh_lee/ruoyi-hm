import { Rule, popMsg } from './Rule'

/**
 * 远程校验
 */
export class RemoteRule implements Rule {
  async validate(val: any, rule: any, form: any): Promise<boolean> {
    let func = rule['asyncValidator']
    if (func && typeof func === 'function') {
      let valid = await func(val, rule, form)
      if (valid) {
        return true
      } else {
        popMsg(rule['errorMessage'])
        return false
      }
    }
    return true
  }
}