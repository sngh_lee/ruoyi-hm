import { Rule, popMsg } from './Rule'
import { Log } from '../log';
/**
 * 正则表达式类型校验
 */
export class RegExRule implements Rule {
  async validate(val: any, rule: any, form: any): Promise<boolean> {
    let exp = rule['pattern']
    if (exp) {
      try {
        if (val instanceof Array) {
          Log.error('此校验规则不支持数组类型忽略', val)
          return true;
        }
        let regEx = new RegExp(exp)
        if (!regEx.test(val)) {
          popMsg(rule['errorMessage'])
          return false;
        }
      } catch (e) {
        Log.error('校验规则非正则表达式忽略', exp, e)
      }
    }
    return true;
  }
}