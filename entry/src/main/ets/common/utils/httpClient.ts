import axios, {
  AxiosRequestConfig,
  InternalAxiosRequestConfig,
  AxiosResponse,
  AxiosError,
  FormData,
  AxiosProgressEvent,
  CreateAxiosDefaults,
  AxiosInstance,
  AxiosRequestHeaders
} from '@ohos/axios'
import { Log } from './log';

export interface ExtAxiosRequestConfig extends AxiosRequestConfig {
  isToken?: boolean
}

export class HttpClient {
  public instance: AxiosInstance;

  constructor(config?: CreateAxiosDefaults<any>) {
    this.instance = axios.create(config);
  }

  // Generic request
  public async request<T>(config?: ExtAxiosRequestConfig): Promise<T> {
    const response: AxiosResponse<T> = await this.instance.request<T>(config);
    return this.formatResponse<T>(response);
  }

  // Method to format the Axios response to ApiResponse
  private formatResponse<T>(response: AxiosResponse<T>): T {
    Log.info('http请求客户端返回：', response.data)
    return response.data;
  }
}