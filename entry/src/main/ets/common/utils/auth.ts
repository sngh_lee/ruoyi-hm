import router from '@ohos.router';
import { getToken } from '../store';
import { Log } from '../../common/utils/log'
/**
 * 判断是否登录并自动跳转登录页面
 */
export function validLogin() {
  let token = getToken();
  Log.info('判断是否登录：', token)
  if (!token) {
    router.replaceUrl({
      url: 'pages/LoginPage'
    });
  }
}