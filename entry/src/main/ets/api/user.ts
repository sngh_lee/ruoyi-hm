import service from '../common/utils/request'
import { upload } from '../common/utils/upload'

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return service.request({
    url: '/system/user/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 查询用户个人信息
export function getUserProfile<T>() {
  return service.request<T>({
    url: '/system/user/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return service.request({
    url: '/system/user/profile',
    method: 'put',
    data: data
  })
}

// 用户头像上传
export function uploadAvatar(name, fileName, buf, context, callback?: Function) {
  let api = '/system/user/profile/avatar'
  return upload(name, fileName, buf, api, context, true, callback)
}