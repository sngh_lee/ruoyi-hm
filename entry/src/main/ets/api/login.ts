import service from '../common/utils/request'

// 登录方法
export function login<T>(username: string, password: string, code: string, uuid: string) {
  const data = {
    username,
    password,
    code,
    uuid,
  };
  return service.request<T>({
    url: '/login',
    isToken: false,
    method: 'post',
    data: data,
  });
}

// 注册方法
export function register(data: any) {
  return service.request({
    url: '/register',
    isToken: false,
    method: 'post',
    data: data,
  });
}

// 获取用户详细信息
export function getInfo<T>() {
  return service.request<T>({
    url: '/getInfo',
    method: 'get',
  });
}

// 退出方法
export function logout() {
  return service.request({
    url: '/logout',
    method: 'post',
  });
}

// 获取验证码
export function getCodeImg<T>() {
  return service.request<T>({
    url: '/captchaImage',
    isToken: false,
    method: 'get',
  });
}